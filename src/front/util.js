const chunk = (arr, size) => {
   let chunked = []
   while (arr.length) {
      chunked.push(arr.splice(0, size))
   }
   return chunked
}

const popNElements = (arr, amount) => {
   return Array(amount)
      .fill()
      .map(item => item = arr.pop())
}

const shuffle = (arr) => {
   let a = [...arr]
   let j, x, i
   for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1))
      x = a[i]
      a[i] = a[j]
      a[j] = x
   } return a
}

export { chunk, popNElements, shuffle }
