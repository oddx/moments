import express from 'express'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import config from '../back.js'
import urls from '../front.js'

const app = express()

const cors = function (req, res, next) {
   //only allow client url
   res.header('Access-Control-Allow-Origin', urls.clientUrl)
   res.header('Access-Control-Allow-Methods', 'GET,OPTIONS,PUT,POST,DELETE')
   res.header('Access-Control-Allow-Headers', 'Content-Type')
   res.header('Access-Control-Allow-Credentials', true)
   return next()
}

const auth = async (req, res) => {
   //verify cookies
   if (req.cookies && req.cookies.isAdmin) {
      try { await jwt.verify(req.cookies.isAdmin, config.jwtSecret) }
      catch (e) { res.status(401).send(e) }
      res.status(200).send()
   } else res.status(401).send()
}

const login = async (req, res) => {
   //receive credentials, compare hashes, send cookie token
   if (req.body.user === config.adminUser &&
      await bcrypt.compare(req.body.pass, config.adminPass)) {
         
      const token = jwt.sign(
         {user: req.body.user}, config.jwtSecret, {expiresIn: '30 minutes'}
      )
      console.dir('logged in ' + req.body.user)
      res.cookie('isAdmin', token, {maxAge: 3600000, httpOnly: false}).status(200).send()
   } else res.status(401).send()
}

export {auth, cors, login}
