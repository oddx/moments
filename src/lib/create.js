import express from 'express'
import fs from 'fs'
import yaml from 'js-yaml'
import config from '../back.js'
import { genName, readType, updateRootMeta, writeMedia } from './util.js'

const createPost = (req, res) => {
   const doc = {
      title:   req.body.title,
      posted:  new Date().getTime()
   }
   const path  = 'albums/' + req.params.album + '/'
   const uri   = path + config.mediaPath
   
   if (!fs.existsSync(uri)) {
         fs.mkdirSync(uri, { recursive: true }) }

   fs.writeFileSync(path + 'meta.yaml',yaml.safeDump(doc))
   
   if (req.files) {
      try {
         req.files.map(file => writeMedia(uri, file))
         updateRootMeta(path, req.body.title)
         
         console.dir('posted album')
         res.status(200).send()
      } catch (err) { res.status(400).send(err) }
   } else res.status(200).send()
}

export { createPost }
