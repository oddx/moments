import express from 'express'
import fs from 'fs'
import yaml from 'js-yaml'
import config from '../back.js'

const deletePost = (req, res) => {
   //drop post from collection,
   const post = req.params.post
   try {
      if (fs.existsSync('albums/' + post)) {
         let order = yaml.safeLoad(fs.readFileSync('albums/meta.yaml'))
         let title = yaml.safeLoad(fs.readFileSync('albums/' + post+ '/meta.yaml')).title
         let newOrder = order.filter(item => item !== title)
         fs.writeFileSync('albums/meta.yaml', yaml.safeDump(newOrder))
         fs.rmdirSync('albums/' + post, { recursive: true })
         console.dir('deleted ' + post)
      } else { console.dir('album ' + post + ' does not exist') }
      res.status(200).send()
   } catch (err) { res.status(400).send(err) }
}

export { deletePost }
