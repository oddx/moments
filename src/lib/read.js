import express from 'express'
import fs from 'fs'
import yaml from 'js-yaml'
import config from '../back.js'
import { prependParent, readMime } from './util.js'

const readAllMedia = async (req, res) => {
   //get first x post titles
   const albums = fs.readdirSync('albums/')
      .filter(item => item !== 'meta.yaml')
   const allMedia = albums
      .map(item => prependParent(
         fs.readdirSync('albums/' + item + '/media/'),
         item,
         '/'))
      .filter(item => item.length)
      .flat()
      .filter(item => item.includes('-thumbnail.'))
   console.log(allMedia)
   res.status(200).json(allMedia)
}

const readPostCount = async (req, res) => {
   //get post count
   const count = fs.readdirSync('albums/').length
   res.status(200).send({'count': count})
}

const readPosts = async (req, res) => {
   //get first x post titles
   const doc = yaml.safeLoad(fs.readFileSync('albums/meta.yaml'))
   console.log(doc)
   res.status(200).json(doc)
}

const readOnePost = async (req, res) => {
   //get post and associated media paths
   let doc = yaml.safeLoad(
      fs.readFileSync('albums/' + req.params.album + '/meta.yaml')
   )
   doc.media = fs.readdirSync('albums/' + req.params.album + '/' + config.mediaPath)
                  .filter(item => !item.includes('-thumbnail.'))
   console.log(doc)
   res.status(200).send(doc)
}

const readPostMedia = (req, res) => {
   //get associated post files from filesystem
   const mime = readMime(req.params.media)
   const media = fs.readFileSync(
      'albums/' +
      req.params.album + '/' +
      config.mediaPath +
      req.params.media
   )
   console.log('found ' + req.params.media)
   res.status(200).set('Content-Type', mime).send(media)
}

export { readAllMedia, readOnePost, readPosts, readPostCount, readPostMedia }
