import fs from 'fs'
import yaml from 'js-yaml'
import config from '../back.js'
import { insertThumbnail, updateMetaTitle, updateRootMeta, writeMedia } from '../lib/util.js'

//update post title
//update post media
const updatePostTitle = (req, res) => {
   const sourcePath = 'albums/' + req.params.source + '/'
   const targetPath = 'albums/' + req.params.target.replaceAll(' ', '-').toLowerCase() + '/'
   try {
      fs.renameSync(sourcePath, targetPath)
      updateRootMeta(targetPath, req.params.target)
      updateMetaTitle(req.params.target.replaceAll(' ', '-').toLowerCase(), req.params.target)
      res.status(200).send()
   } catch (err) { res.status(400).send(err) }
}

const updatePost = (req, res) => {
   //delete files in 'media' array
   //upload files from req.files
   const media       = typeof req.body.media == 'object' ?
                       req.body.media.map(insertThumbnail).flat() :
                       insertThumbnail(req.body.media)
   const path        = 'albums/' + req.params.album + '/'
   const mediaPath   = path + config.mediaPath
   const deleteFiles = fs.readdirSync(mediaPath)
                        .filter(filename => !media.includes(filename))
   let   doc         = yaml.safeLoad(fs.readFileSync(path + 'meta.yaml'))

   doc.updated = (new Date()).getTime()
               
   if (deleteFiles) {
      updateRootMeta(path, doc.title)
      deleteFiles.forEach(media => {
         fs.unlinkSync(mediaPath + media) })
   }

   if (req.files) {
      if (!fs.existsSync(mediaPath)) fs.mkdirSync(mediaPath)

      req.files.map(file => writeMedia(mediaPath, file))
      fs.writeFileSync(path + 'meta.yaml',yaml.safeDump(doc))
      updateRootMeta(path, doc.title)
      console.dir('updated album')
   } else res.status(200).send()
   res.status(200).send()
}

export { updatePost, updatePostTitle }
