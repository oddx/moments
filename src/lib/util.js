import fs from 'fs'
import sharp from 'sharp'
import yaml from 'js-yaml'

const prependParent = (arr, parentItem, separator) => {
   return arr.map(item => parentItem + separator + item)
}

const updateRootMeta = (postPath, title) => {
   let oldTitle = yaml.safeLoad(fs.readFileSync(postPath + 'meta.yaml')).title
   let order = yaml.safeLoad(fs.readFileSync('albums/meta.yaml'))
                   .filter(item => item != oldTitle)
   order.reverse()
   order.push(title)
   order.reverse()
   console.dir(order)
   fs.writeFileSync('albums/meta.yaml', yaml.safeDump(order))
}

const updateMetaTitle = (post, title) => {
   const metaPath = 'albums/' + post + '/meta.yaml'
   let doc = yaml.safeLoad(fs.readFileSync(metaPath))
   doc.title = title
   console.dir(doc)
   fs.writeFileSync(metaPath, yaml.safeDump(doc))
}

const writeMedia = (uri, file) => {
   const srcFile    = uri + genName() + readType(file.mimetype)
   const targetFile = uri + genName() + readType(file.mimetype)
   fs.writeFileSync(srcFile, file.buffer)
   resizeMedia(srcFile, targetFile)
   genThumbnail(srcFile, targetFile, 748, 421)
}

const resizeMedia = (src, target) => {
   //4k max dimensions
   sharp(src)
      .resize(3840, 3840, {fit: 'inside'})
      .toFile(target,
      err => {
         if (err) console.dir(err)
         else { console.dir('added ' + target) }
      })
}

const genThumbnail = (src, target, width, height) => {
   //calc dimensions from image size on 4k screen
   const thumbUri = target.split('.').shift() +
                        '-thumbnail.' +
                        target.split('.').pop()
   sharp(src)
      .resize(width, height, {fit: 'inside'})
      .toFile(thumbUri,
      err => {
         if (err) console.dir(err)
         else {
            fs.unlinkSync(src)
            console.dir('added ' + thumbUri)
         }
      })
}

const insertThumbnail = (item) => {
   if (item === undefined) return []
   return [item, item.split('.').shift() + '-thumbnail.' + item.split('.').pop()]
}

const genName = () => {
   return (new Date()).getTime().toString(36) +
      Math.random().toString(36).slice(2)
}

const readType = (mime) => {
   if      (mime === 'image/png')  return '.png'
   else if (mime === 'image/jpeg') return '.jpg'
}

const readMime = (file) => {
   const type = file.split('.').pop()
   if      (type === 'png') return 'image/png'
   else if (type === 'jpg') return 'image/jpeg'
}

export { genName, insertThumbnail, prependParent, readMime,
         readType, updateMetaTitle, updateRootMeta, writeMedia }
