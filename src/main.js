import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import Edit from './Edit.vue'
import App from './App.vue'
import Home from './Home.vue'
import Login from './Login.vue'
import Album from './Album.vue'
import config from './front.js'

Vue.use(VueResource);
Vue.use(VueRouter);

const auth = (from, to, next) => {
   if (document.cookie && from.path !== '/login') {
      fetch(config.serverUrl + 'auth', { credentials: 'include' })
         .then(data => {
            if (data.status != 200) next('/login')
            else return next()
      }).catch(console.dir)
   } else if (from.path === '/login') {
     return next()
   } else {
     return next('/login')
   }   
}

const router = new VueRouter({
   mode: 'history',
   routes: [
      {
         path: '/',
         component: Home
      },
      {
         path: '/login',
         component: Login,
         beforeEnter: auth
      },
      {
         path: '/albums/:album',
         component: Album,
         props: true
      },
      {
         path: '/edit/:album',
         component: Edit,
         props: true,
         beforeEnter: auth
      },
      {
         path: '/edit',
         component: Edit,
         beforeEnter: auth
      }
   ]
})

new Vue({
   el: '#app',
   router,
   render: h => h(App)
})
