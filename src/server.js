import multer from 'multer'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import compression from 'compression'
import express from 'express'
import fs from 'fs'
import yaml from 'js-yaml'
import config from './back.js'
import { auth, cors, login } from './lib/auth.js'
import { readAllMedia, readOnePost, readPosts, readPostCount, readPostMedia } from './lib/read.js'
import { createPost } from './lib/create.js'
import { updatePost, updatePostTitle } from './lib/update.js'
import { deletePost } from './lib/delete.js'

const app = express()
const upload = multer({storage: multer.memoryStorage()})

app.use(cors,
   express.json(),
   bodyParser.json(),
   cookieParser(),
   compression()
)

app.get('/api/auth/',   auth)
app.post('/api/login/', login)

app.get('/api/posts/',        readPosts)
app.get('/api/posts/count',   readPostCount)
app.get('/api/all-media',     readAllMedia)
app.get('/api/albums/:album', readOnePost)
app.get('/api/:album/:media', readPostMedia)

app.post('/api/albums/:album', upload.array('photos'), createPost)

app.post('/api/edit/:album',   upload.array('photos'), updatePost)
app.post('/api/rename/:source/:target',                updatePostTitle)

app.delete('/api/:post', deletePost)

app.get('/api/greet', (req, res) => {
   console.dir('greetings!')
   res.status(200).send({message: 'heyo'})
})

app.listen(config.backPort, console.dir('Check port ' + config.backPort))
